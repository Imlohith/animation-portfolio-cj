function splitScreen() {
    const controller = new ScrollMagic.Controller()

    new ScrollMagic.Scene({
        duration: "200%",
        triggerElement: '.about-title',
        triggerHook: 0
    })
    .setPin('.about-title')
    .addIndicators()
    .addTo(controller)
}

splitScreen();

particlesJS.load('particles-js', 'assets/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });

  var slideUp = {
    distance: '200%',
    origin: 'left',
    opacity: null,
    duration: 2000,
    rotate: {
        x: 100,
        z: 100
    }
};

var slideTop = {
    distance: '100%',
    origin: 'top',
    duration: 1000,
}



  ScrollReveal().reveal('.header', slideUp);


  ScrollReveal().reveal('.proj1', slideUp )
  ScrollReveal().reveal('.proj2', slideTop )
  ScrollReveal().reveal('.proj3', slideTop )

  
var heading = document.querySelector(".heading");
heading.innerHTML = heading.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline()
  .add({
    targets: '.heading .letter',
    opacity: [0,1],
    easing: "easeInOutQuad",
    duration: 2250,
    delay: (el, i) => 150 * (i+1)
  })